const express = require('express')
const spark = require('./spark')
var http = require('http')

const PORT = 3001

const app = express()
app.all('/', (request, response, next) => {
    console.log('Spark Auth API')
    response.send('Spark Auth API')
})

app.all('/auth/spark', spark.authorize)
app.all('/auth/spark/callback', spark.callback)

http.createServer(app).listen(PORT)