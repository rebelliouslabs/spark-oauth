const axios = require('axios')

const CLIENT_ID = 'a6wg7fq1uz5o4mzm04bjrpo1r'
const SCOPE = 'openid'
const HOST = 'https://6a639048.ngrok.io' // ngroc url


exports.authorize = (request, response, next) => {
    const response_type = 'POST'
    const state = ''
    const spark_host = 'https://sparkplatform.com'
    const redirect_uri = `${HOST}/auth/spark/callback`

    console.log({CLIENT_ID, spark_host, response_type, redirect_uri, SCOPE, state})

    response.redirect(`${spark_host}/openid/authorize`
        + `?client_id=${CLIENT_ID}`
        + `&scope=${SCOPE}`
        + `&response_type=${response_type}`
        + `&redirect_uri=${redirect_uri}`
        + `&state=${state}`)
}

exports.callback = (request, response, next) => {
    console.log(request)
    const {client_id, client_secret, grant_type, code} = request
    const redirect_uri = 'https://sparkplatform.com/oauth2/callback'

    const data = {
        client_id,
        client_secret,
        grant_type,
        code
    }

    console.log({redirect_uri, data})

    axios.post(redirect_uri, data)
        .then(response => {
            console.log(response)
            next()
        })
        .catch(error => {
            console.error(error)
            next()
        }) 
}